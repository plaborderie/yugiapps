import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Accueil from "../components/Accueil";
import Navigation from "../components/Navbar";
import Calculateur from "../components/apps/Calculateur/Calculateur";
import Decks from "../components/apps/Decks/Decks";

const AppRouter = props => (
  <BrowserRouter basename="/yugiapps">
    <div>
      <Navigation />
      <Switch>
        <Route path="/" component={Accueil} exact />
        <Route path="/calculateur" component={Calculateur} />
        <Route path="/decks" component={Decks} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;
