import React, { Component } from "react";
import { Container } from "reactstrap";
import Paragraph from "./CustomComponents/Paragraph";
import PageTitle from "./CustomComponents/PageTitle";

class Accueil extends Component {
  render() {
    return (
      <div>
        <PageTitle
          title="Yu-Gi-Apps"
          subtitle="Des applications pour faciliter votre vie de duelliste"
        />
        <Container>
          <Paragraph
            title="Un différent type de jeu de cartes"
            text="Les jeux de cartes à collectionner sont différents des jeux de cartes classiques : chaque jeu à ses propres cartes.
                    Les parties se déroulent sous forme de duels, où deux joueurs s'affrontent en 1 contre 1 à l'aide des paquets de
                    cartes qu'ils ont construit au préalable. Le but est d'utiliser ses cartes pour réduire les points de vies de l'adversaire
                    à 0."
          />
          <Paragraph
            title="Utilisez nos applications"
            text="Nous concevons des applications destinées à tous les joueurs afin de s'assurer que les duels
                    restent amusants. L'objectif est d'avoir une application pour gérer chaque partie de la vie de duelliste, comme la
                    création de deck ou le calcul de points de vie.
                    Ainsi, vous pouvez vous concentrer sur les duels et nous laisser nous occuper de l'organisation."
          />
          <Paragraph
            title="La construction de deck"
            text="Un des aspects principaux des jeux de cartes à collectionner est la construction de deck.
                    Deck est le nom donné aux paquets de cartes (souvent entre 40 et 60 cartes) individuel à chaque joueur,
                     qu'il construit en collectionnant des cartes. Notre application permet de
                     choisir les cartes que l'on souhaite utiliser dans un deck, et le prix total est calculé. Cela permet
                     de choisir une composition de deck en fonction de son budget."
          />
          <Paragraph
            title="Le calculateur de points de vie"
            text="L'objectif principal de Yu-Gi-Oh! est de réduire les points de vie de son adversaire à 0. Notre calculateur
                    permet de ne pas avoir à garder trace manuellement de toutes les attaques réalisées durant la partie pour savoir où en est
                    le duel. Les joueurs peuvent donc se concentrer sur le duel sans avoir à se soucier des calculs !"
          />
        </Container>
      </div>
    );
  }
}

export default Accueil;
