import React from "react";
import {
  Navbar,
  NavbarToggler,
  Collapse,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

import { NavLink as RRNavLink } from "react-router-dom";

class Navigation extends React.Component {
  state = {
    collapsed: true,
    parties: [
      {
        id: 1,
        title: "Accueil",
        path: "/",
        exact: true
      },
      {
        id: 2,
        title: "Calculateur",
        path: "/calculateur"
      },
      {
        id: 3,
        title: "Constructeur de deck",
        path: "/decks"
      }
    ]
  };
  toggleNavbar = () => {
    this.setState(prevState => ({ collapsed: !prevState.collapsed }));
  };
  render() {
    return (
      <div className="mb-5">
        <Navbar
          color="white"
          fixed="top"
          light
          expand
          className="d-none d-md-flex"
        >
          <NavbarBrand to="/" tag={RRNavLink}>
            Yu-Gi-Apps
          </NavbarBrand>
          <Nav className="ml-auto" navbar>
            {this.state.parties.map(partie => (
              <NavItem key={partie.id}>
                <NavLink
                  to={partie.path}
                  activeClassName="active"
                  tag={RRNavLink}
                  exact={partie.exact}
                >
                  {partie.title}
                </NavLink>
              </NavItem>
            ))}
          </Nav>
        </Navbar>
        {/* Mobile navbar*/}
        <Navbar color="white" light fixed="top" className="d-flex d-sm-none">
          <NavbarBrand to="/" tag={RRNavLink}>
            Yu-Gi-Apps
          </NavbarBrand>
          <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
          <Collapse isOpen={!this.state.collapsed} navbar>
            <Nav className="ml-auto" navbar>
              {this.state.parties.map(partie => (
                <NavItem key={partie.id}>
                  <NavLink
                    to={partie.path}
                    activeClassName="active"
                    tag={RRNavLink}
                    exact={partie.exact}
                  >
                    {partie.title}
                  </NavLink>
                </NavItem>
              ))}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Navigation;
