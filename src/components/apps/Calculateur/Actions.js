import React from 'react';
import { Row, Col, Button } from 'reactstrap';

export default (props) => (
  <div>
    <Row>
      <Col xs="6">
        <Button 
          block 
          color="primary"
          onClick={props.inflictDamage}
        >
          Infliger dégâts
        </Button>
      </Col>
      <Col xs="6">
        <Button 
          block 
          color="primary"
          onClick={props.heal}
        >
          Soigner
        </Button>
      </Col>
    </Row>
    <Row className="mt-3">
      <Col xs="6">
        <Button 
          block 
          color="danger"
          onClick={props.resetSelection}
        >
          Annuler sélection
        </Button>
      </Col>
      <Col xs="6">
        <Button 
          block 
          color="danger"
          onClick={props.resetGame}
        >
          Réinitialiser partie
        </Button>
      </Col>
    </Row>
    
  </div>
);