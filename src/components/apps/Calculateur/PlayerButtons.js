import React from 'react';
import { Row, Col, Button } from 'reactstrap';

export default (props) => (
  <Row>
    {
      props.players.map((player) => (
        <Col key={player.id}>
          <Button 
            outline 
            block
            color="primary" 
            active={player.isTarget}
            onClick={(e) => {props.changeTarget(player.id)}}
          >
            {player.name} : {player.lifepoints}
          </Button>
        </Col>
      ))
    }
  </Row>
);