import React from 'react';
import { Row, Col, Button } from 'reactstrap';

export default (props) => (
  <Row>
    {
      props.buttons.map((buttonValue) => (
        <Col xs="4" key={buttonValue}>
          <Button 
            block 
            outline
            color="primary"
            onClick={(e) => {props.changeSelection(buttonValue)}}
            className="mb-3"
          >
            {buttonValue}
          </Button>
        </Col>
      ))
    }
  </Row>
);