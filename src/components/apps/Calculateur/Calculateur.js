import React from 'react';
import PageTitle from '../../CustomComponents/PageTitle';
import { Container, Row, Col } from 'reactstrap';
import PlayerButtons from './PlayerButtons';
import NumberButtons from './NumberButtons';
import Actions from './Actions';

class Calculateur extends React.Component {
  state = {
    players: [
      {
        id: 1,
        name: 'Joueur 1',
        lifepoints: 8000,
        isTarget: true
      },
      {
        id: 2,
        name: 'Joueur 2',
        lifepoints: 8000,
        isTarget: false
      }
    ],
    buttons: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '00', '000'],
    selectedAmount: ''
  };
  changeTarget = (id) => {
    const players = this.state.players;
    players.forEach((player) => {
      if (id === player.id) {
        player.isTarget = true;
      } else {
        player.isTarget = false;
      }
    });
    this.setState(() => ({ players }));
  };
  changeSelection = (amount) => {
    this.setState((prevState) => ({ selectedAmount: prevState.selectedAmount + amount }))
  };
  resetSelection = () => {
    this.setState(() => ({ selectedAmount: '' }));
  };
  resetGame = () => {
    const players = this.state.players.map((player) => ({
      ...player,
      lifepoints: 8000
    }));
    this.setState(() => ({ players }));
  };
  inflictDamage = () => {
    const players = this.state.players;
    const damageValue = this.state.selectedAmount;
    players.forEach((player) => {
      if(player.isTarget && damageValue) {
        player.lifepoints -= parseInt(damageValue, 10);
      }
    });
    this.resetSelection();
    this.setState(() => ({ players }));
  }
  heal = () => {
    const players = this.state.players;
    const healValue = this.state.selectedAmount;
    players.forEach((player) => {
      if(player.isTarget && healValue) {
        player.lifepoints += parseInt(healValue, 10);
      }
    });
    this.resetSelection();
    this.setState(() => ({ players }));
  }
  render() {
    return (
      <div className="mb-3">
        <PageTitle title="Calculateur de points de vie" />
        <Container>
          <Row>
            <Col xs="12" md={{size: 8, offset: 2}} lg={{size: 6, offset: 3}}>
              <PlayerButtons players={this.state.players} changeTarget={this.changeTarget} />
              <p className="text-center my-3">
                Sélection : {this.state.selectedAmount}
              </p>
              <NumberButtons buttons={this.state.buttons} changeSelection={this.changeSelection} />
              <Actions 
                resetSelection={this.resetSelection}
                resetGame={this.resetGame} 
                inflictDamage={this.inflictDamage}
                heal={this.heal}
              />
            </Col>
          </Row>
        </Container>

      </div>
    );
  }
}

export default Calculateur;