import React from "react";
import { Modal, ModalHeader, ModalBody, Button, Input, Row } from "reactstrap";

export default class ExportModal extends React.Component {
  handleCopyExport = () => {
    document.getElementById("copyText").select();
    document.execCommand("copy");
    this.props.handleToggleModal();
  };
  render() {
    return (
      <Modal
        isOpen={this.props.exportModal}
        toggle={this.props.handleToggleModal}
      >
        <ModalHeader toggle={this.props.handleToggleModal}>
          Exporter le deck
        </ModalHeader>
        <ModalBody className="mx-2">
          <Row>
            <Input
              id="copyText"
              value={this.props.getCardList()}
              readOnly
              className="mb-3"
            />
          </Row>
          <Row>
            <Button color="primary" onClick={this.handleCopyExport} block>
              Copier
            </Button>
          </Row>
        </ModalBody>
      </Modal>
    );
  }
}
