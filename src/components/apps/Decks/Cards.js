import React from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  ButtonGroup,
  Table,
  Collapse
} from "reactstrap";
import AddCard from "./AddCard";
import Divider from "../../CustomComponents/Divider";

const Cards = props => (
  <div>
    <Row>
      <Col md={{ size: 8, offset: 2 }}>
        <Card className="mb-5">
          <CardBody>
            <Row>
              <CardTitle className="ml-1">
                {props.deck.name} (${props.totalPrice})
              </CardTitle>
              <div className="ml-auto pb-2">
                <ButtonGroup>
                  <Button
                    outline
                    color="danger"
                    onClick={e => props.handleRemoveDeck(props.deck.id)}
                  >
                    <i className="fas fa-trash-alt" />
                  </Button>
                  <Button
                    outline
                    color="primary"
                    onClick={props.handleShowDeck}
                  >
                    {props.collapse ? (
                      <i className="fas fa-eye" />
                    ) : (
                      <i className="fas fa-eye-slash" />
                    )}
                  </Button>
                </ButtonGroup>
              </div>
            </Row>
            <Row>
              <div className="ml-auto pb-2">
                <ButtonGroup>
                  <Button
                    color="primary"
                    outline
                    onClick={props.handleToggleImportModal}
                  >
                    Importer deck
                  </Button>
                  <Button
                    color="primary"
                    outline
                    onClick={props.handleToggleExportModal}
                  >
                    Exporter Deck
                  </Button>
                </ButtonGroup>
              </div>
            </Row>
            <Collapse isOpen={props.collapse}>
              <Divider text="COMPOSITION DU DECK" />

              <AddCard
                handleAddCard={props.handleAddCard}
                testValue={10}
                cardList={props.cardList}
              />
              {props.cards.length > 0 ? (
                <Table className="mt-3">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nom</th>
                      <th>Prix</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {props.cards.sort(props.sortByName).map(card => (
                      <tr key={card.name}>
                        <th scope={"row"}>{card.quantity}</th>
                        <td>
                          <a href={card.link} target="_blank">
                            {card.name}
                          </a>
                        </td>
                        <td>
                          ${parseFloat(card.price * card.quantity).toFixed(2)}
                        </td>
                        <td>
                          <ButtonGroup size="sm">
                            <Button
                              color="default"
                              outline
                              disabled={card.quantity >= 3}
                              onClick={e => props.handleAddCard(card.name)}
                            >
                              <i className="fas fa-plus-circle" />
                            </Button>
                            <Button
                              color="default"
                              outline
                              onClick={e => props.handleRemoveCard(card.name)}
                            >
                              <i className="fas fa-minus-circle" />
                            </Button>
                          </ButtonGroup>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              ) : (
                <p>
                  Saisissez le nom de la carte et appuyer sur le bouton pour
                  rajouter une carte.
                </p>
              )}
            </Collapse>
          </CardBody>
        </Card>
      </Col>
    </Row>
  </div>
);

export default Cards;
