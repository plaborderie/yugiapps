import React from "react";
import { Button, Modal, ModalHeader, ModalBody, Input, Row } from "reactstrap";

export default class DeckModal extends React.Component {
  handleImportDeck = e => {
    e.preventDefault();
    const deck = document.getElementById("importDeckInput").value;
    if (typeof deck === "string") {
      this.props.handleImportDeck(deck);
    } else {
      console.log("Wrong input!");
    }
    document.getElementById("importDeckInput").value = "";
    this.props.handleToggleModal();
  };

  render() {
    return (
      <Modal
        isOpen={this.props.importModal}
        toggle={this.props.handleToggleModal}
      >
        <ModalHeader toggle={this.props.handleToggleModal}>
          Importer un deck
        </ModalHeader>
        <ModalBody>
          <form
            onSubmit={this.handleImportDeck}
            autoComplete="off"
            className="mx-2"
          >
            <Row>
              <Input
                id="importDeckInput"
                placeholder="Code du deck"
                className="mb-3"
              />
            </Row>
            <Row>
              <Button color="success" type="submit" block>
                Importer deck
              </Button>
            </Row>
          </form>
        </ModalBody>
      </Modal>
    );
  }
}
