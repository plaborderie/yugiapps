import React from "react";
import {
  InputGroup,
  InputGroupAddon,
  Input,
  Button,
  Row,
  Col
} from "reactstrap";

class AddDeck extends React.Component {
  handleAddDeck = e => {
    e.preventDefault();
    const deckName = document.getElementById("deckNameInput").value;
    if (typeof deckName === "string") {
      this.props.handleAddDeck(deckName);
    } else {
      console.log("Wrong input!");
    }
    document.getElementById("deckNameInput").value = "";
  };

  render() {
    return (
      <Row>
        <Col md={{ size: 8, offset: 2 }}>
          <form onSubmit={this.handleAddDeck} autoComplete="off">
            <InputGroup className="mb-3">
              <Input placeholder="Nom du deck" id="deckNameInput" />
              <InputGroupAddon addonType="append">
                <Button color="success" type="submit">
                  Créer deck
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </form>
        </Col>
      </Row>
    );
  }
}

export default AddDeck;
