import React from "react";
import PageTitle from "../../CustomComponents/PageTitle";
import { Container, Alert, Row, Col } from "reactstrap";
import AddDeck from "./AddDeck";
import Deck from "./Deck";

export default class Decks extends React.Component {
  state = {
    decks: [],
    cardList: [],
    createdDeckAlert: false,
    removedDeckAlert: false
  };

  getCardList = () => {
    var request = new XMLHttpRequest();

    request.open("GET", "https://www.ygohub.com/api/all_cards");

    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        if (request.status === 200) {
          const cardList = JSON.parse(request.responseText).cards.map(card => ({
            value: card,
            label: card
          }));
          this.setState(() => ({ cardList }));
        }
      }
    };

    request.send();
  };

  showAddDeckAlert = () => {
    this.setState(() => ({ createdDeckAlert: true }));
    setTimeout(() => {
      this.setState(() => ({ createdDeckAlert: false }));
    }, 3000);
  };

  showRemoveDeckAlert = () => {
    this.setState(() => ({ removedDeckAlert: true }));
    setTimeout(() => {
      this.setState(() => ({ removedDeckAlert: false }));
    }, 3000);
  };

  handleAddDeck = deckName => {
    const id =
      this.state.decks.length > 0
        ? this.state.decks[this.state.decks.length - 1].id + 1
        : 0;
    const decks = this.state.decks.concat({
      name: deckName,
      id
    });
    this.setState(() => ({ decks }));
    localStorage.setItem("decks", JSON.stringify(decks));
    this.showAddDeckAlert();
  };

  handleRemoveDeck = id => {
    const decks = this.state.decks.filter(e => e.id !== id);
    this.setState(() => ({ decks }));
    localStorage.setItem("decks", JSON.stringify(decks));
    localStorage.removeItem(id);
    this.showRemoveDeckAlert();
  };

  componentDidMount() {
    this.getCardList();
    if (localStorage.hasOwnProperty("decks")) {
      const decks = JSON.parse(localStorage.getItem("decks"));
      this.setState(() => ({ decks }));
    }
  }

  render() {
    return (
      <div>
        <PageTitle
          title="Constructeur de deck"
          subtitle="Créez des decks et estimez leur prix total !"
        />

        <Container>
          <AddDeck handleAddDeck={this.handleAddDeck} />
          <Row>
            <Col md={{ size: 8, offset: 2 }}>
              <Alert isOpen={this.state.createdDeckAlert} color="success">
                Nouveau deck créé !
              </Alert>
            </Col>
          </Row>
          <Row>
            <Col md={{ size: 8, offset: 2 }}>
              <Alert isOpen={this.state.removedDeckAlert} color="danger">
                Deck supprimé !
              </Alert>
            </Col>
          </Row>
          {this.state.decks.length > 0 ? (
            this.state.decks.map(deck => (
              <Deck
                key={deck.id}
                deck={deck}
                cardList={this.state.cardList}
                handleRemoveDeck={this.handleRemoveDeck}
              />
            ))
          ) : (
            <h3 className="text-center mt-2">
              Ajoutez un deck pour commencer !
            </h3>
          )}
        </Container>
      </div>
    );
  }
}
