import React from "react";
import { Button, Row, Col } from "reactstrap";

// Autocompletion
import Select from "react-virtualized-select";
import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";

export default class AddCard extends React.Component {
  state = {
    selectedCard: undefined
  };

  handleAddCard = () => {
    if (this.state.selectedCard) {
      this.props.handleAddCard(this.state.selectedCard.value);
    }
  };

  onChange = selectedCard => {
    this.setState(() => ({ selectedCard }));
  };

  render() {
    return (
      <div>
        <Row>
          <Col>
            <Row>
              <Col span={8}>
                <Select
                  value={this.state.selectedCard}
                  onChange={this.onChange}
                  options={this.props.cardList}
                  optionHeight={60}
                  placeholder="Nom de la carte"
                />
              </Col>
              <Button color="success" onClick={this.handleAddCard}>
                Ajouter
              </Button>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}
