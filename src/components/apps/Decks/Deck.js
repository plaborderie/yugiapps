import React from "react";
import Cards from "./Cards";
import ImportModal from "./importDeckModal";
import ExportModal from "./ExportDeckModal";
const axios = require("axios");

class Deck extends React.Component {
  state = {
    cards: [],
    totalPrice: 0,
    collapse: false,
    importModal: false,
    exportModal: false
  };

  getCardList = () => {
    let deck = { cards: [] };
    this.state.cards.forEach(card => {
      deck.cards.push({ name: card.name, quantity: card.quantity });
    });
    return JSON.stringify(deck);
  };

  handleCopyExport = copyText => {
    copyText.select();
    document.execCommand("copy");
  };

  handleToggleExportModal = () => {
    this.setState(prevState => ({ exportModal: !prevState.exportModal }));
  };

  handleToggleImportModal = () => {
    this.setState(prevState => ({ importModal: !prevState.importModal }));
  };

  handleImportDeck = importedDeck => {
    let reqUrl = "";
    const deck = JSON.parse(importedDeck);
    const cards = deck.cards;
    if (cards.length > 0) {
      cards.forEach(card => {
        for (let i = 0; i < (card.quantity ? card.quantity : 1); i++) {
          if (reqUrl.length >= 0) {
            reqUrl += "&";
          }
          reqUrl += "cards=" + encodeURI(card.name);
        }
      });
      axios
        .get(`https://yugiapps-deck-api.appspot.com?${reqUrl}`)
        .then(response => {
          this.setState(() => ({ cards: [], totalPrice: 0 }));
          const cards = response.data.cards;
          cards.forEach(card => {
            this.handleAddCard(card);
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  handleShowDeck = () => {
    this.setState({ collapse: !this.state.collapse });
  };

  handleAddCard = cardName => {
    //Adding card and changing state

    let duplicate = false;
    let cards = this.state.cards; //Copying cards
    cards.forEach(card => {
      // If card already exists, add it's quantity by one and set duplicate to true
      if (card.name.toLowerCase() === cardName.toLowerCase()) {
        if (card.quantity < 3) {
          card.quantity++;
        } else {
          this.setState(prevState => ({
            totalPrice: prevState.totalPrice - card.price
          }));
        }
        duplicate = true;
      }
    });
    // Checking if card is duplicate, if not, add it
    if (!duplicate) {
      cards.push({ name: cardName, quantity: 1, price: undefined });
    }
    this.setState(() => ({
      cards
    }));

    var request = new XMLHttpRequest();

    let price = undefined;

    let link = undefined;

    request.open(
      "GET",
      `https://www.ygohub.com/api/card_info?name=${cardName.replace(
        /&/g,
        "%26"
      )}`
    );

    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        if (request.status === 200) {
          if (JSON.parse(request.responseText).status === "success") {
            price = parseFloat(
              JSON.parse(request.responseText).card.price_low
            ).toFixed(2);
            link = JSON.parse(request.responseText).card.tcgplayer_link;
            cards.forEach(card => {
              if (card.name.toLowerCase() === cardName.toLowerCase()) {
                card.price = price;
                card.link = link;
              }
            });
          } else {
            cards.forEach(card => {
              if (
                card.name.toLowerCase() === cardName.toLowerCase() &&
                card.quantity > 0
              ) {
                card.price = "0";
                card.link = "#";
              }
            });
          }

          this.setState(prevState => ({
            cards,
            totalPrice: (
              parseFloat(prevState.totalPrice) + parseFloat(price)
            ).toFixed(2)
          }));
        }
      }
    };

    request.send();
  };

  handleRemoveCard = cardName => {
    let cards = this.state.cards;
    let price = "0";
    cards.forEach(card => {
      if (card.name === cardName) {
        card.quantity--;
        price = card.price;
      }
      if (card.quantity === 0) {
      }
    });
    cards = cards.filter(card => card.quantity > 0);
    this.setState(prevState => ({
      cards,
      totalPrice: (
        parseFloat(prevState.totalPrice) - parseFloat(price)
      ).toFixed(2)
    }));
  };

  sortByName = (a, b) => {
    const nameA = a.name.toLowerCase();
    const nameB = b.name.toLowerCase();

    let compare;

    nameA > nameB ? (compare = 1) : (compare = -1);

    return compare;
  };

  componentDidMount() {
    if (localStorage.hasOwnProperty(this.props.deck.id)) {
      const state = JSON.parse(localStorage.getItem(this.props.deck.id));
      this.setState(() => ({
        cards: state.cards,
        totalPrice: state.totalPrice,
        collapse: state.collapse
      }));
    }
  }

  componentDidUpdate() {
    localStorage.setItem(this.props.deck.id, JSON.stringify(this.state));
  }

  render() {
    return (
      <div>
        <ImportModal
          importModal={this.state.importModal}
          handleImportDeck={this.handleImportDeck}
          handleToggleModal={this.handleToggleImportModal}
        />
        <ExportModal
          exportModal={this.state.exportModal}
          handleToggleModal={this.handleToggleExportModal}
          getCardList={this.getCardList}
          handleCopyExport={this.handleCopyExport}
        />
        <Cards
          deck={this.props.deck}
          cards={this.state.cards}
          cardList={this.props.cardList}
          totalPrice={this.state.totalPrice}
          collapse={this.state.collapse}
          handleToggleImportModal={this.handleToggleImportModal}
          handleToggleExportModal={this.handleToggleExportModal}
          handleShowDeck={this.handleShowDeck}
          handleAddCard={this.handleAddCard}
          handleRemoveCard={this.handleRemoveCard}
          handleRemoveDeck={this.props.handleRemoveDeck}
          sortByName={this.sortByName}
          getTotalPrice={this.state.getTotalPrice}
        />
      </div>
    );
  }
}

export default Deck;
