import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default () => (
  <Navbar color="dark" expand className="mt-5 text-light" id="footer">
    <NavbarBrand>Paul Laborderie - 2018</NavbarBrand>
    <Nav className="ml-auto" navbar>
      <NavItem>
        <NavLink href="https://github.com/plaborderie" target="_blank" className="text-light">GITHUB</NavLink>
      </NavItem>
    </Nav>
  </Navbar>
);