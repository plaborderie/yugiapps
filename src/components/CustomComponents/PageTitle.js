import React from 'react';
import { Container, Row, Col, Jumbotron } from 'reactstrap';

const PageTitle = (props) => (
  <Jumbotron fluid className="bg-dark">
      <Container>
          <Row>
              <Col>
                  <h1 className="text-center text-light">{props.title}</h1>
                  { props.subtitle && <h5 className="text-center text-light">{props.subtitle}</h5>}
              </Col>
          </Row>
      </Container>
  </Jumbotron>
);

export default PageTitle;
