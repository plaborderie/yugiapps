import React from "react";
import { Row, Col } from "reactstrap";

const Paragraph = props => (
  <div>
    <Row>
      <Col className="mb-3" md={{ size: 8, offset: 2 }}>
        <h5 className="text-center">{props.title}</h5>
      </Col>
    </Row>
    <Row>
      <Col className="mb-5" md={{ size: 8, offset: 2 }}>
        <p className="text-justify">
          <span className="mx-3" />
          {props.text}
        </p>
      </Col>
    </Row>
  </div>
);

export default Paragraph;
