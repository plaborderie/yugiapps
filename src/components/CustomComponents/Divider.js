import React from "react";
import { Col, Row } from "reactstrap";

export default props => (
  <Row>
    <Col>
      <hr />
    </Col>
    <Col xs="auto">{props.text}</Col>
    <Col>
      <hr />
    </Col>
  </Row>
);
